## Instructions

Please read these instructions carefully and follow them the best you can. If you get stuck email us--there's a ```mailto:``` link at the bottom of these instructions to do that.

## About the application - Todo's

Below is a simple ToDo application that we would like to you build, Following are the [user stories](https://www.mountaingoatsoftware.com/agile/user-stories):

1. As a user, I should be able to view a page with a task list on it.
2. As a user, I should be able to add a new task to the task list.
3. As a user, I should be able to mark a task in the list as completed.

This is single user application and do not worry about cross-user scenarios or security in that respect.

A task is simply defined as a small piece of work with a description and can exist in one of two simple states: completed or not completed. As for the list, consider it empty any time someone first visits your index page; you don't have to worry about saving the state of your application between uses.

### How to start

1. Fork this repository into your own Bitbucket branch.
2. Solve the problem below, checking in to your branch as you go as if this were a real project you're working on. Note that we're also testing your familiarity with Bitbucket/Git. We will look at your commit history closely.
3. Submit a pull request back to this repository with your solution when finished.

### Architecture

1. Implement the project with ASP.NET MVC. 
2. Implement back end with ASP.NET Web API which would be the Service layer. Persist data however you want.

### OPTIONAL
1. Please use JavaScript/Jquery if required on the front end. 
2. Make sure you write unit tests for the code using MOQ for .NET.


Please email questions to: code@credomobile.com.